import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'personal-details',
      component: () => import('../views/PersonalDetails.vue')
    },
    {
      path: '/payment-details',
      name: 'payment-details',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/PaymentDetails.vue')
    }
  ]
})

export default router
