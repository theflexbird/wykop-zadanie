import { defineStore } from 'pinia'

export const useCheckoutStore = defineStore('counter', {
  state: () => ({
    step: 1,
    stepName: "Personal Information",
    formData: {}
  }),
  getters: {
    getStepName(state) {
      return state.stepName
    },
    getStep(state) {
      return state.step
    },
    getFormData(state) {
      return state.formData
    }
  },
  actions: {
    setStepName(name: string) {
      this.stepName = name
    },
    setStep(step: number) {
      this.step = step
    },
    setFormData(data: object) {
      this.formData = data
    }
  }
});
